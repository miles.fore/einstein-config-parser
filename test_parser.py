import unittest

from parse import parse_config


class ParserTest(unittest.TestCase):

    def test_happy_path(self):
        test_config_expected = {
            'host': 'test.com',
            'server_id': 55331,
            'server_load_alarm': 2.5,
            'user': 'user',
            'verbose': True,
            'test_mode': True,
            'debug_mode': False,
            'log_file_path': '/tmp/logfile.log',
            'send_notifications': True
        }

        # verify that both dictionaries contain the same items
        parsed_config = parse_config('test_config.config')
        for k, v in parsed_config.items():
            assert k in test_config_expected.keys()
            self.assertEqual(test_config_expected[k], v)

        for k in test_config_expected.keys():
            assert k in parsed_config.keys()

    def test_bad_config(self):
        with self.assertRaises(Exception):
            parsed_config = parse_config('bad_config.config')


if __name__ == '__main__':
    unittest.main()
