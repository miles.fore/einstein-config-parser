from argparse import ArgumentParser

import sys
from parse import parse_config

config_dict = parse_config('test_config.config')


def check_load(server_load):
    if server_load >= config_dict.get('server_load_alarm', 0):
        print(f"Warning: server {config_dict.get('server_id')} is above the load alarm of "
              f"{config_dict.get('server_load_alarm')}")
        if config_dict.get('send_notifications'):
            print(f"Sending notification to {config_dict.get('user')}")


if __name__ == '__main__':
    arg_parser = ArgumentParser('Triggers alarm if server load is above the alarm level')
    arg_parser.add_argument('load', help='Current server load as int or float')
    args = arg_parser.parse_args()

    try:
        server_load = float(args.load)
    except ValueError:
        print("load must be an int or float")
        sys.exit(1)

    check_load(server_load=server_load)
