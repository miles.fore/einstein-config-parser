string_boolean_map = {
    'on': True,
    'off': False,
    'yes': True,
    'no': False,
    'true': True,
    'false': False
}


def infer_type(string):
    """Infers type from the string and returns the string cast as the appropriate type. e.g. '3' returns 3,
    'true' returns True, 'test' returns 'test'"""
    # return boolean if in string map
    if string.lower() in string_boolean_map.keys():
        return string_boolean_map[string.lower()]

    # Try to convert to int and float, if both fail return string
    try:
        return int(string)
    except ValueError:
        pass

    try:
        return float(string)
    except ValueError:
        pass

    return string


def parse_config(config_file):
    """Parses config file and returns a dictionary representing the values in the config. Blank lines and
    commented lines are ignored. Error in parsing raises an exception.
    :param config_file (str) - the file to be read (must be in path or full path specified)
    :returns (dict) - dictionary with key/value pairs as specified in the config file"""

    # Will raise FileNotFoundError exception if can't find file
    with open(config_file, 'r') as f:
        lines = f.readlines()

    rtn_dict = {}
    for line_no, line in enumerate(lines):
        # start line_no at index at 1
        line_no += 1

        # ignore blank or commented lines
        line = line.strip()
        if line != '' and line[0] != '#':
            line_split = line.split("=", 1)

            # valid config lines should have two items here
            if len(line_split) != 2:
                raise Exception(f"Error parsing line {line_no}")
            else:
                rtn_dict[line_split[0].strip()] = infer_type(line_split[1].strip())

    return rtn_dict
